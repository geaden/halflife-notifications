# -*- coding: utf-8 -*-
import gdata.calendar.client
import gdata.gauth


client = gdata.calendar.client.CalendarClient(source='yourCo-yourAppName-v1')
client.ClientLogin('denisovgena@gmail.com', 'qqxwglnupvfnjtga', client.source)


def GetAuthSubUrl():
    next = 'http://localhost:8080'
    scopes = ['https://www.google.com/calendar/feeds/']
    secure = False  # set secure=True to request a secure AuthSub token
    session = True
    return gdata.gauth.generate_auth_sub_url(next, scopes, secure=secure, session=session)


def AddReminder(calendar_client, event, minutes=10):
  for a_when in event.when:
    if len(a_when.reminder) > 0:
      a_when.reminder[0].minutes = minutes
    else:
      a_when.reminder.append(gdata.data.Reminder(minutes=minutes))

  print 'Adding %d minute reminder to event' % (minutes,)
  return calendar_client.Update(event)


def InsertSingleEvent(calendar_client, title='One-time Tennis with Beth',
                      content='Meet for a quick lesson', where='On the courts',
                      start_time=None, end_time=None):
    event = gdata.calendar.data.CalendarEventEntry()
    event.title = atom.data.Title(text=title)
    event.content = atom.data.Content(text=content)
    event.where.append(gdata.calendar.data.CalendarWhere(value=where))

    if start_time is None:
      # Use current time for the start_time and have the event last 1 hour
      start_time = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.gmtime())
      end_time = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.gmtime(time.time() + 3600))
    event.when.append(gdata.calendar.data.When(start=start_time, end=end_time))

    new_event = calendar_client.InsertEvent(event)

    print 'New single event inserted: %s' % (new_event.id.text,)
    print '\tEvent edit URL: %s' % (new_event.GetEditLink().href,)
    print '\tEvent HTML URL: %s' % (new_event.GetHtmlLink().href,)

    return new_event


def PrintUserCalendars(client):
    feed = client.GetAllCalendarsFeed()
    print feed.title.text
    for i, a_calendar in enumerate(feed.entry):
        print '\t%s. %s' % (i, a_calendar.title.text,)

