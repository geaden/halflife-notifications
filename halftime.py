#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import jinja2
import os
from datetime import datetime

import gdata
import gdata.data
import gdata.service
import gdata.alt.appengine

import time

import logging
import simplejson

import google_data

import atom

import gdata.calendar

from google.appengine.ext import db
from google.appengine.api import users

from utils import get_all_notifications


# Define template dir.
template_dir = os.path.join(
    os.path.dirname(__file__),
    'templates')
# Define jinja environment.
jinja_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(template_dir),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


def get_halftasks():
    return list(db.GqlQuery(
        'SELECT * FROM HalfTask ORDER BY created DESC'
    ))


class StoredToken(db.Model):
    user_email = db.StringProperty(required=True)
    session_token = db.StringProperty(required=True)


class HalfTask(db.Model):
    """
    Entity for handling event
    """
    name = db.StringProperty()
    deadline = db.DateTimeProperty()
    settime = db.DateTimeProperty()
    created = db.DateTimeProperty(auto_now_add=True)

    @property
    def get_notifications(self):
        return list(Notification.gql(
            "WHERE halftask = :1",
            self.key()
        ))

class Notification(db.Model):
    """
    Entity for storing notifications for event
    """
    halftask = db.ReferenceProperty(HalfTask)
    title = db.StringProperty(required=True)
    description = db.TextProperty()
    time = db.DateTimeProperty()
    location = db.TextProperty()
    creator = db.StringProperty()
    edit_link = db.TextProperty()
    gcal_event_link = db.TextProperty()
    gcal_event_xml = db.TextProperty()
    notified = db.BooleanProperty()


class MainHandler(webapp2.RequestHandler):
    """
    Basic handler for web application.
    """
    # def __init__(self):
    #     # Create a Google Calendar client to talk to the Google Calendar service.
    #     self.calendar_client = gdata.calendar.service.CalendarService()
    #     # Modify the client to search for auth tokens in the datastore and use
    #     # urlfetch instead of httplib to make HTTP requests to Google Calendar.
    #     gdata.alt.appengine.run_on_appengine(self.calendar_client)
    user = users.get_current_user()
    login_url = users.create_login_url(*args)
    logout_url = users.create_logout_url(*args)


    def write(self, *args, **kwargs):
        """
        Write out in response provided args and kwargs
        """
        self.response.out.write(*args, **kwargs)

    def render_str(self, template, **params):
        """
        Rendering provided kwargs into template
        """
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kwargs):
        """
        Render page
        """
        self.write(self.render_str(template, **kwargs))

    def initialize(self, *args, **kwargs):
        """
        Initializing Blog Handler.
        """
        webapp2.RequestHandler.initialize(self, *args, **kwargs)


class HalfTimePage(MainHandler):
    def get(self):
        """Displays the events the user has created or is invited to."""
        # For brevity, left out the code from above which found events in the datastore.
        if user:
            ht = get_halftasks()
            success=''

            if 'success' in self.request.arguments():
                success="Task created!"

        self.render(
            'index.html',
            ht=ht,
            success=success,
            user=user
        )


class CreateHalfTaskPage(MainHandler):
    def get(self):
        self.render(
            'create_ht.html'
        )

    def post(self):
        name = self.request.get('name')
        deadline = self.request.get('deadline')
        settime = self.request.get('settime')

        ht = HalfTask(
            name=name,
            deadline=datetime.strptime(deadline, '%Y-%m-%d'),
            settime=datetime.strptime(settime, '%Y-%m-%d')
        ).put()
        self.redirect('/?success')


class GetNotification(MainHandler):
    def get(self, ht_id):
        key = db.Key.from_path('HalfTask', int(ht_id))
        halftask = db.get(key)
        redirect_url = '/halftask/{0}'.format(ht_id)
        if halftask.get_notifications:
            self.redirect(redirect_url)
        else:
            for date in get_all_notifications(halftask.deadline, halftask.settime):
                Notification(
                    halftask=halftask,
                    title="Notification for {0}".format(halftask.name),
                    time=date
                ).put()
            self.response.headers['Content-Type'] = 'text/plain'
            self.response.out.write("Generating notifications...")
            time.sleep(10)
            self.redirect(redirect_url)


class HalfTaskPage(MainHandler):
    def get(self, ht_id):
        key = db.Key.from_path('HalfTask', int(ht_id))
        halftask = db.get(key)
        notifications = halftask.get_notifications
        self.render(
            'halftaskpage.html',
            halftask=halftask,
            notifications=notifications
        )


class Notified(MainHandler):
    def post(self, id_notification):
        key = db.Key.from_path('Notification', int(id_notification))
        notification = db.get(key)
        if self.request.body == 'true':
            res = True
        else:
            res = False
        notification.notified = res
        notification.put()


class AddToGoogCalendar(MainHandler):
    # def __init__(self):
    #     # Create a Google Calendar client to talk to the Google Calendar service.
    #     self.calendar_client = gdata.calendar.service.CalendarService()
    #     # Modify the client to search for auth tokens in the datastore and use
    #     # urlfetch instead of httplib to make HTTP requests to Google Calendar.
    #     gdata.alt.appengine.run_on_appengine(self.calendar_client)


    def get(self, id_halftask):
        key = db.Key.from_path('HalfTask', int(id_halftask))
        halftask = db.get(key)
        redirect_url = '/halftask/{0}'.format(id_halftask)
        notifications = halftask.get_notifications
        client = google_data.client
        # gdata.alt.appengine.run_on_appengine(client)
        feed = client.GetOwnCalendarsFeed()
        # map the 'title' -> 'url'
        urls = dict((e.title.text, e.content.src) for e in feed.entry)
        calendar = urls['Job']
        minutes = 10

        for notification in notifications:
            event = gdata.calendar.CalendarEventEntry()
            logging.info("EVENT: %s" % event)
            logging.info("EVENT TYPE: %s" % type(event))
            event.title = atom.Title(text=notification.title)
            event.content = atom.Content(text='{0}'.format(redirect_url))
            start_time = '%s.00z' % notification.time.isoformat()
            event.when.append(gdata.calendar.When(start_time=start_time))

            for a_when in event.when:
                if len(a_when.reminder) > 0:
                    a_when.reminder[0].minutes = minutes
                else:
                    a_when.reminder.append(gdata.data.Reminder(minutes=minutes))

            # Send the event information to Google Calendar and receive a
            # Google Calendar event.
            try:
                cal_event = client.InsertEvent(
                    event,
                    calendar
                )
                edit_link = cal_event.GetEditLink()
                if edit_link and edit_link.href:
                    # Add the edit link to the event to use for making changes.
                    notification.edit_link = edit_link.href
                alternate_link = cal_event.GetHtmlLink()
                if alternate_link and alternate_link.href:
                    # Add a link to the event in the Google Calendar HTML web UI.
                    event.gcal_event_link = alternate_link.href
                    event.gcal_event_xml = str(cal_event)
                event.put()
            # If adding the event to Google Calendar failed due to a bad auth token,
            # remove the user's auth tokens from the datastore so that they can
            # request a new one.
            except gdata.service.RequestError, request_exception:
                request_error = request_exception[0]
                if request_error['status'] == 401 or request_error['status'] == 403:
                    gdata.alt.appengine.save_auth_tokens({})
                # If the request failure was not due to a bad auth token, reraise the
                # exception for handling elsewhere.
                else:
                    raise

        self.redirect(redirect_url)



app = webapp2.WSGIApplication([
                                    ('/?', HalfTimePage),
                                    ('/create/?', CreateHalfTaskPage),
                                    ('/genotifications/(\d+)/?', GetNotification),
                                    ('/halftask/(\d+)/?', HalfTaskPage),
                                    ('/notified/(\d+)/?', Notified),
                                    ('/addevent/(\d+)/?', AddToGoogCalendar)
                              ], debug=True)
