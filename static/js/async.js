/**
 * Created with PyCharm.
 * User: geaden
 * Date: 2/20/13
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */
//
// As mentioned at http://en.wikipedia.org/wiki/XMLHttpRequest
//
if( !window.XMLHttpRequest ) XMLHttpRequest = function()
{
    try{ return new ActiveXObject("Msxml2.XMLHTTP.6.0") }catch(e){}
    try{ return new ActiveXObject("Msxml2.XMLHTTP.3.0") }catch(e){}
    try{ return new ActiveXObject("Msxml2.XMLHTTP") }catch(e){}
    try{ return new ActiveXObject("Microsoft.XMLHTTP") }catch(e){}
    throw new Error("Could not find an XMLHttpRequest alternative.")
};

function Notify(el) {
    // Create an XMLHttpRequest 'POST' request
    param = el.checked;
    var body = JSON.stringify(param);
    var req = new XMLHttpRequest();
    req.open('POST', '/notified/' + el.id);
    req.send(body);
};
