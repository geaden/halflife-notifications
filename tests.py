import unittest

from utils import get_all_notifications, get_next_notification, convert_str_to_date

from google.appengine.ext import testbed, db

from halftime import HalfTask, get_halftasks, Notification

from datetime import datetime


class BaseTestCase(unittest.TestCase):
    """
    Base Test Case for all tests
    """
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Declare which service stubs you want to use.
        # I use init_datastore_v3_stub() method. With
        # no argument uses an in-memory datastore that is initially empty.
        # If you want to test an existing datastore entity,
        # include its pathname
        # as an argument to init_datastore_v3_stub().
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()


class HalftimeTestCase(unittest.TestCase):
    def test_get_next_notification(self):
        control_date = convert_str_to_date('22.02.2013')
        task_received = convert_str_to_date('20.02.2013')
        notification = convert_str_to_date('21.02.2013')
        self.assertEqual(
            notification,
            get_next_notification(
                control_date,
                task_received)
            )

    def test_all_notifications(self):
        control_date = convert_str_to_date('11.03.2013')
        task_received = convert_str_to_date('19.02.2013')
        self.assertEqual(
            5,
            len(list(get_all_notifications(control_date, task_received)))
        )


class CreateHalfTask(BaseTestCase):
    def test_create_half_task(self):
        times = 5
        for i in range(times):
            HalfTask(
                name='foo',
                deadline=datetime.today()
            ).put()
        self.assertEqual(
            times,
            len(get_halftasks())
        )

    def test_get_notifications(self):
        halftask = HalfTask(
            name='foo',
            deadline=datetime.today()
        )
        halftask.put()
        Notification(
            halftask=halftask,
            title='foo'
        ).put()
        self.assertEqual(
            len(list(Notification.all())),
            1
        )
        self.assertEqual(
            len(list(halftask.get_notifications)),
            1
        )

    def test_gen_notifications(self):
        halftask = HalfTask(
            name='foo',
            deadline=datetime(2013, 2, 19),
            settime=datetime(2013, 3, 11)
        )
        halftask.put()
        for date in get_all_notifications(halftask.deadline, halftask.settime):
            Notification(
                halftask=halftask,
                title="Notification for {0}".format(halftask.name),
                time=date
            ).put()
        self.assertEqual(
            len(list(halftask.get_notifications)),
            len(list(get_all_notifications(halftask.deadline, halftask.settime)))
        )


if __name__ == "__main__":
     unittest.main()


