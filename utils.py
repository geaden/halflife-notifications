# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Gennady Denisov.
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.

from datetime import datetime, timedelta
from constants import MASK


def convert_str_to_date(datestring):
    """
    Coverts string to date in given mask
    """
    return datetime.strptime(datestring, MASK)


def convert_date_to_str(date):
    """
    Converts date to human readable string
    """
    return date.strftime(MASK)


def get_next_notification(control_date, last_notification):
    """
    Get time of next notification.
    :param control_date: Control date, that task should be acheived
    :param last_notification: Time when task was notified last time
    :type control_date: datetime
    :type last_notification: datetime
    :return next_notification
    :rtype datetime
    """
    next_notification = last_notification + \
                        timedelta((control_date - last_notification).days / 2)
    return next_notification


def get_all_notifications(control_date, last_notification):
    """
    Get all notifications for current task
    """
    while (control_date - last_notification).days > 1:
        next_notification = get_next_notification(
            control_date,
            last_notification
        )
        last_notification = next_notification
        yield last_notification